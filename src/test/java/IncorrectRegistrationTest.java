import config.Config;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectRegistrationTest extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongPasswords() {
        return new Object[][]{
                {"Abcdef!", "Passwords must have at least one digit ('0'-'9')."},
                {"Abcdef1", "Passwords must have at least one non alphanumeric character."},
                {"abcdef1!", "Passwords must have at least one uppercase ('A'-'Z')."}
        };
    }

    @Test(dataProvider = "getWrongPasswords")
    public void incorrectEmailTest(String wrongPassword, String expectedError) {

        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail(new Config().getApplicationUser())
                .typePassword(wrongPassword)
                .confirmPassword(wrongPassword)
                .registerWithFailure()
                .assertCorrectErrorMessagesAreShown(expectedError);
    }

}