import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class MenuTest extends SeleniumBaseTest {

    @Test
    public void testMenu() {
        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .assertProcessesUrl(new Config().getApplicationProcessesUrl())
                .assertProcessesHeader()
                .goToCharacteristics()
                .assertCharacteristicsUrl(new Config().getApplicationCharacteristicsUrl())
                .assertCharacteristicsHeader()
                .goToDashboard()
                .assertDashboardUrl(new Config().getApplicationUrl())
                .assertDemoProjectIsShown();
    }

}
