import config.Config;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.time.Duration;

public class SeleniumBaseTest {

    protected WebDriver driver;

    @BeforeMethod
    public void baseBeforeMethod() {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();

        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        driver.manage().window().setSize(new Dimension(1920, 1080));

        driver.get(new Config().getApplicationUrl());
    }

    @AfterMethod
    public void baseAfterMethod() {
//        driver.quit();
    }

}