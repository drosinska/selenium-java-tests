import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectProcessAdditionTest extends SeleniumBaseTest {

    @Test
    public void addProcessWithFailureTest() {

        String shortProcessName = "ab";
        String expErrorMessage = "The field Name must be a string with a minimum length of 3 and a maximum length of 30.";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(shortProcessName)
                .submitCreateWithFailure()
                .assertProcessNameError(expErrorMessage)
                .backToList()
                .assertProcessIsNotShown(shortProcessName);

    }
}
