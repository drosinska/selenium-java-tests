import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

public class CorrectLoginTest extends SeleniumBaseTest {

    @Test
    public void correctLoginTest() {
        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .assertWelcomeElementIsShown();
    }

}