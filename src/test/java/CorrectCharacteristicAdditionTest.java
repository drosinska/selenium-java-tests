import config.Config;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class CorrectCharacteristicAdditionTest extends SeleniumBaseTest {

    @Test
    public void addCharacteristic() {

        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "10";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword(new Config().getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLowerSpecificationLimit(lsl)
                .typeUpperSpecificationLimit(usl)
                .submitCreate()
                .assertCharacteristic(characteristicName, lsl, usl, "");
    }

}
