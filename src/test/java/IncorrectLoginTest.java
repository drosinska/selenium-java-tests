import config.Config;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class IncorrectLoginTest extends SeleniumBaseTest {

    @DataProvider
    public Object[][] getWrongEmails() {
        return new Object[][]{
                {"test"},
                {"admin"},
                {"@test"}
        };
    }

    @Test(dataProvider = "getWrongEmails")
    public void incorrectLoginTestWrongEmail(String wrongEmail) {

        String expErrorMessage = "The Email field is not a valid e-mail address.";

        new LoginPage(driver)
                .typeEmail(wrongEmail)
                .typePassword(new Config().getApplicationPassword())
                .submitLoginWithFailure()
                .assertEmailErrorIsShown(expErrorMessage);
    }

    @Test
    public void incorrectLoginTestWrongPassword() {

        String expErrorMessage = "Invalid login attempt.";

        new LoginPage(driver)
                .typeEmail(new Config().getApplicationUser())
                .typePassword("test")
                .submitLoginWithFailure()
                .assertLoginErrorIsShown(expErrorMessage);
    }

}