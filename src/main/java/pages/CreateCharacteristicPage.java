package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateCharacteristicPage extends HomePage {

    public CreateCharacteristicPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ProjectId")
    private WebElement selectProcessList;

    @FindBy(id = "Name")
    private WebElement nameTxt;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerSpecificationLimitTxt;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSpecificationLimitTxt;

    @FindBy(css = "input[type=submit]")
    private WebElement createBtn;

    @FindBy(linkText = "Back to List")
    private WebElement backToListBtn;

    @FindBy(css = ".field-validation-error[data-valmsg-for=ProjectId]")
    private WebElement selectProcessError;

    public CreateCharacteristicPage selectProcess(String projectName) {
        new Select(selectProcessList).selectByVisibleText(projectName);

        return this;
    }

    public CreateCharacteristicPage typeName(String characteristicName) {
        nameTxt.clear();
        nameTxt.sendKeys(characteristicName);

        return this;
    }

    public CreateCharacteristicPage typeLowerSpecificationLimit(String lowerSpecificationLimit) {
        lowerSpecificationLimitTxt.clear();
        lowerSpecificationLimitTxt.sendKeys(lowerSpecificationLimit);

        return this;
    }

    public CreateCharacteristicPage typeUpperSpecificationLimit(String upperSpecificationLimit) {
        upperSpecificationLimitTxt.clear();
        upperSpecificationLimitTxt.sendKeys(upperSpecificationLimit);

        return this;
    }

    public CharacteristicsPage submitCreate() {
        createBtn.click();

        return new CharacteristicsPage(driver);
    }

    public CreateCharacteristicPage submitCreateWithFailure() {
        createBtn.click();

        return this;
    }

    public CharacteristicsPage backToList() {
        backToListBtn.click();

        return new CharacteristicsPage(driver);
    }

    public CreateCharacteristicPage assertProcessError(String expError) {
        Assert.assertEquals(selectProcessError.getText(), expError);

        return this;
    }
}