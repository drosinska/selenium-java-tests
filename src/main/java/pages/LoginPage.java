package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class LoginPage {

    protected WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".flash-message>strong")
    private WebElement logoutMsg;

    @FindBy(css = "a[href*=Register]")
    private WebElement registerLnk;

    @FindBy(id = "Email")
    private WebElement emailTxt;

    @FindBy(id = "Email-error")
    public WebElement emailError;

    @FindBy(id = "Password")
    private WebElement passwordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement loginBtn;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> loginErrors;

    public HomePage login(String email, String password) {
        typeEmail(email);
        typePassword(password);
        submitLogin();

        return new HomePage(driver);
    }

    public LoginPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);

        return this;
    }

    public LoginPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);

        return this;
    }

    public HomePage submitLogin() {
        loginBtn.click();

        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure() {
        loginBtn.click();

        return this;
    }

    public CreateAccountPage goToRegisterPage() {
        registerLnk.click();

        return new CreateAccountPage(driver);
    }

    public String getEmailError() {
        return emailError.getText();
    }

    public LoginPage assertLoginErrorIsShown2(String errorText) {
        Assert.assertTrue(loginErrors.stream().anyMatch( error -> error.getText().equals(errorText)));
        return this;
    }

    public LoginPage assertLoginErrorIsShown(String errorText) {
        boolean doesErrorExists = false;
        for (WebElement loginError : loginErrors) {
            if (loginError.getText().equals(errorText)) {
                doesErrorExists = true;
                break;
            }
        }
        Assert.assertTrue(doesErrorExists);

        return this;
    }

    public LoginPage assertEmailErrorIsShown(String expError){
        Assert.assertTrue(emailError.isDisplayed());
        Assert.assertEquals(emailError.getText(), expError);

        return this;
    }

    public LoginPage assertUserSuccessfullyLogOut() {
        Assert.assertTrue(logoutMsg.isDisplayed());
        Assert.assertEquals(logoutMsg.getText(), "User succesfully logged out");

        return this;
    }
}